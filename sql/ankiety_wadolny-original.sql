-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 09, 2018 at 11:48 PM
-- Server version: 10.1.29-MariaDB
-- PHP Version: 7.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ankiety_wadolny`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `dodaj_ankiete_do_bazy` (`vNazwa` VARCHAR(255))  begin
INSERT INTO ankiety(id_ankiety, nazwa) VALUES(NULL, vNazwa);
SELECT "DONE" as "Status" , "1" as "Bool", "dodaj_ankiete_do_bazy" as "Fun","Ankieta dodana do bazy" as "Info";
end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `dodaj_odpowiedz` (`vNIU` INT, `vOdpowiedz` INT)  begin
INSERT INTO odpowiedzi_uzytkownicy(id_odpowiedzi, id_uzytkownika) VALUES(vOdpowiedz, vNIU);
UPDATE odpowiedzi SET ilosc_odpowiedzi = ilosc_odpowiedzi + 1 WHERE id_odpowiedzi = vOdpowiedz;
SELECT "DONE" as "Status" , "1" as "Bool", "dodaj_odpowiedz" as "Fun","Odpowiedź dodana do bazy" as "Info";
end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `dodaj_odpowiedz_do_bazy` (`vPytanieNO` INT, `vTresc` VARCHAR(255))  begin
SET @n1 = (SELECT tresc from pytania where id_pytania = vPytanieNo);
if (@n1 is NOT NULL) then
	SET @n2 = (SELECT max(id_odpowiedzi) from odpowiedzi) + 1;
	INSERT INTO odpowiedzi(id_odpowiedzi, odpowiedz, ilosc_odpowiedzi) VALUES(@n2, vTresc, 0);
	INSERT INTO pytania_odpowiedzi(id_pytania, id_odpowiedzi) VALUES(vPytanieNO, @n2);
	SELECT "DONE" as "Status" , "1" as "Bool", "dodaj_odpowiedz_do_bazy" as "Fun","Odpowiedź dodana do bazy" as "Info";
else
	SELECT "ERROR" as "Status" , "0" as "Bool", "dodaj_odpowiedz_do_bazy" as "Fun","Odpowiedź nie została dodana do bazy" as "Info";
end if;
end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `dodaj_pytanie_do_bazy` (`vAnkietaNO` INT, `vTresc` VARCHAR(255))  begin
SET @n1 = (SELECT dostepnosc from ankiety where id_ankiety = vAnkietaNo);
if (@n1 is NOT NULL) then
	SET @n2 = (SELECT max(id_pytania) from pytania) + 1;
	INSERT INTO pytania(id_pytania, tresc) VALUES(@n2, vTresc);
	INSERT INTO ankiety_pytania(id_ankiety, id_pytania) VALUES(vAnkietaNO, @n2);
	SELECT "DONE" as "Status" , "1" as "Bool", "dodaj_pytanie_do_bazy" as "Fun","Pytanie dodane do bazy" as "Info";
else
	SELECT "ERROR" as "Status" , "0" as "Bool", "dodaj_pytanie_do_bazy" as "Fun","Pytanie nie zostało dodane do bazy" as "Info";
end if;
end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `pobierz_NIU` ()  begin
SET @n1 = (SELECT max(id_uzytkownika) from uzytkownicy) + 1;
INSERT INTO uzytkownicy(id_uzytkownika) VALUES(@n1);
SELECT "DONE" as "Status" , "1" as "Bool", "pobierz_NIU" as "Fun","User added correctly" as "Info", @n1 as "niu";
end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `skasuj_ankiete` (`vAnkietaNO` INT)  begin
DELETE FROM odpowiedzi_uzytkownicy WHERE id_odpowiedzi in (Select id_odpowiedzi FROM ankiety_pytania ap LEFT JOIN pytania_odpowiedzi po on ap.id_pytania = po.id_pytania WHERE ap.id_ankiety = vAnkietaNO);
DELETE FROM odpowiedzi WHERE id_odpowiedzi in (Select id_odpowiedzi FROM ankiety_pytania ap LEFT JOIN pytania_odpowiedzi po on ap.id_pytania = po.id_pytania WHERE ap.id_ankiety = vAnkietaNO);
DELETE FROM pytania_odpowiedzi WHERE id_pytania in (Select id_pytania FROM ankiety_pytania ap WHERE ap.id_ankiety = vAnkietaNO);
DELETE FROM pytania WHERE id_pytania IN (SELECT id_pytania FROM ankiety_pytania WHERE id_ankiety = vAnkietaNO);
DELETE FROM ankiety_pytania WHERE id_ankiety = vAnkietaNO;
DELETE FROM ankiety WHERE id_ankiety = vAnkietaNO;
SELECT "DONE" as "Status" , "1" as "Bool", "skasuj_ankietee" as "Fun","Ankieta, pytania i odpowiedzi skasowane" as "Info";
end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `skasuj_odpowiedz` (`vOdpowiedzNO` INT)  begin
DELETE FROM odpowiedzi_uzytkownicy WHERE id_odpowiedzi = vOdpowiedzNO;
DELETE FROM pytania_odpowiedzi WHERE id_odpowiedzi = vOdpowiedzNO;
DELETE FROM odpowiedzi WHERE id_odpowiedzi = vOdpowiedzNO;
SELECT "DONE" as "Status" , "1" as "Bool", "skasuj_odpowiedz" as "Fun","Odpowiedź skasowana" as "Info";
end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `skasuj_pytanie` (`vPytanieNO` INT)  begin
DELETE FROM odpowiedzi_uzytkownicy WHERE id_odpowiedzi in (Select id_odpowiedzi FROM pytania_odpowiedzi WHERE id_pytania = vPytanieNO);
DELETE FROM odpowiedzi WHERE id_odpowiedzi in (Select id_odpowiedzi FROM pytania_odpowiedzi WHERE id_pytania = vPytanieNO);
DELETE FROM pytania_odpowiedzi WHERE id_pytania = vPytanieNO;
DELETE FROM ankiety_pytania WHERE id_pytania = vPytanieNO;
DELETE FROM pytania WHERE id_pytania = vPytanieNO;
SELECT "DONE" as "Status" , "1" as "Bool", "skasuj_pytanie" as "Fun","Pytanie i odpowiedzi skasowane" as "Info";
end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `toggle_ankieta` (`vAnkietaNO` INT)  begin
SET @n1 = (SELECT dostepnosc from ankiety where id_ankiety = vAnkietaNo);
if (@n1 is NOT NULL) then
	if( @n1 = 0) then
		UPDATE ankiety SET dostepnosc = 1 WHERE id_ankiety = vAnkietaNO;
		SELECT "DONE" as "Status" , "1" as "Bool", "toggle_ankieta" as "Fun","Status ankiety zmieniony" as "Info";
	else
		UPDATE ankiety SET dostepnosc = 0 WHERE id_ankiety = vAnkietaNO;
		SELECT "DONE" as "Status" , "1" as "Bool", "toggle_ankieta" as "Fun","Status ankiety zmieniony" as "Info";
	end if;
else
	SELECT "ERROR" as "Status" , "0" as "Bool", "toggle_ankieta" as "Fun","Status ankiety nie został zmieniony" as "Info";
end if;
end$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `ankiety`
--

CREATE TABLE `ankiety` (
  `id_ankiety` int(10) NOT NULL,
  `nazwa` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `dostepnosc` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Dumping data for table `ankiety`
--

INSERT INTO `ankiety` (`id_ankiety`, `nazwa`, `dostepnosc`) VALUES
(1, 'Ankieta Testowa v1', 1),
(2, 'Ankieta Testowa v2 ze znakami ąśżźćółń', 1),
(8, 'Ankieta numer 3 z 3 pytaniami', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ankiety_pytania`
--

CREATE TABLE `ankiety_pytania` (
  `id_ankiety` int(10) NOT NULL,
  `id_pytania` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Dumping data for table `ankiety_pytania`
--

INSERT INTO `ankiety_pytania` (`id_ankiety`, `id_pytania`) VALUES
(1, 1),
(1, 2),
(2, 3),
(8, 4),
(8, 5),
(8, 6),
(8, 7);

-- --------------------------------------------------------

--
-- Table structure for table `odpowiedzi`
--

CREATE TABLE `odpowiedzi` (
  `id_odpowiedzi` int(10) NOT NULL,
  `odpowiedz` varchar(1024) COLLATE utf8_polish_ci NOT NULL,
  `ilosc_odpowiedzi` int(10) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Dumping data for table `odpowiedzi`
--

INSERT INTO `odpowiedzi` (`id_odpowiedzi`, `odpowiedz`, `ilosc_odpowiedzi`) VALUES
(1, 'Tak', 2),
(2, 'Nie', 1),
(3, 'Może', 2),
(4, 'Treść jest zadowalająca', 0),
(5, 'Treść nie jest zadowalająca', 2),
(6, 'Treść może jest zadowalająca', 1),
(7, 'Treść być może nie jest zadowalająca', 1),
(8, 'To tylko testowe pytanie', 4),
(9, 'Wiec wybierz losowa odpowiedz', 1),
(10, 'ze wszystkich', 0),
(11, '4 mozliwych', 2),
(12, 'Warto robic ankiety', 3),
(13, 'Nie warto robic ankiet', 2),
(14, 'Ankiety sa spoko', 2),
(15, 'Procesory Intel sa najlepsze', 3),
(16, 'Intel forever', 2),
(17, 'Nie bo Meltdown', 1),
(18, 'Nigdy nie byly dobre', 1),
(19, 'I tak lepsze od AMD', 0),
(20, 'AMD zdecydwoanie lepsze', 5),
(21, 'Niestety tylko Intel', 1),
(22, 'AMD lepsze bo Meltdown', 0),
(23, 'AMD tanszy', 1);

-- --------------------------------------------------------

--
-- Table structure for table `odpowiedzi_uzytkownicy`
--

CREATE TABLE `odpowiedzi_uzytkownicy` (
  `id_odpowiedzi` int(11) NOT NULL,
  `id_uzytkownika` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Dumping data for table `odpowiedzi_uzytkownicy`
--

INSERT INTO `odpowiedzi_uzytkownicy` (`id_odpowiedzi`, `id_uzytkownika`) VALUES
(1, 11),
(6, 11),
(3, 13),
(5, 13),
(2, 14),
(5, 14),
(3, 16),
(7, 16),
(11, 20),
(14, 20),
(18, 20),
(20, 20),
(8, 21),
(12, 21),
(15, 21),
(20, 21),
(8, 22),
(12, 22),
(15, 22),
(20, 22),
(9, 23),
(13, 23),
(16, 23),
(21, 23),
(8, 24),
(13, 24),
(17, 24),
(23, 24),
(11, 25),
(14, 25),
(16, 25),
(20, 25),
(8, 26),
(12, 26),
(15, 26),
(20, 26);

-- --------------------------------------------------------

--
-- Table structure for table `pytania`
--

CREATE TABLE `pytania` (
  `id_pytania` int(10) NOT NULL,
  `tresc` varchar(1024) COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Dumping data for table `pytania`
--

INSERT INTO `pytania` (`id_pytania`, `tresc`) VALUES
(1, 'Czy ta ankieta jest spoko?'),
(2, 'Czy treść jest zadowalająca'),
(3, 'Pytanie bez odpowiedzi'),
(4, 'Pytanie testowe nr1'),
(5, 'Czy warto robic ankiety?'),
(6, 'Czy procesory Intel sa dobre?'),
(7, 'Czy procesory AMD sa lepsze?');

-- --------------------------------------------------------

--
-- Table structure for table `pytania_odpowiedzi`
--

CREATE TABLE `pytania_odpowiedzi` (
  `id_pytania` int(10) NOT NULL,
  `id_odpowiedzi` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Dumping data for table `pytania_odpowiedzi`
--

INSERT INTO `pytania_odpowiedzi` (`id_pytania`, `id_odpowiedzi`) VALUES
(1, 1),
(1, 2),
(1, 3),
(2, 4),
(2, 5),
(2, 6),
(2, 7),
(4, 8),
(4, 9),
(4, 10),
(4, 11),
(5, 12),
(5, 13),
(5, 14),
(6, 15),
(6, 16),
(6, 17),
(6, 18),
(6, 19),
(7, 20),
(7, 21),
(7, 22),
(7, 23);

-- --------------------------------------------------------

--
-- Table structure for table `uzytkownicy`
--

CREATE TABLE `uzytkownicy` (
  `id_uzytkownika` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Dumping data for table `uzytkownicy`
--

INSERT INTO `uzytkownicy` (`id_uzytkownika`) VALUES
(1),
(2),
(3),
(4),
(5),
(6),
(7),
(8),
(9),
(10),
(11),
(12),
(13),
(14),
(15),
(16),
(17),
(18),
(19),
(20),
(21),
(22),
(23),
(24),
(25),
(26);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ankiety`
--
ALTER TABLE `ankiety`
  ADD PRIMARY KEY (`id_ankiety`);

--
-- Indexes for table `ankiety_pytania`
--
ALTER TABLE `ankiety_pytania`
  ADD KEY `id_ankiety` (`id_ankiety`),
  ADD KEY `id_pytania` (`id_pytania`);

--
-- Indexes for table `odpowiedzi`
--
ALTER TABLE `odpowiedzi`
  ADD PRIMARY KEY (`id_odpowiedzi`);

--
-- Indexes for table `odpowiedzi_uzytkownicy`
--
ALTER TABLE `odpowiedzi_uzytkownicy`
  ADD KEY `id_odpowiedzi` (`id_odpowiedzi`),
  ADD KEY `id_uzytkownika` (`id_uzytkownika`);

--
-- Indexes for table `pytania`
--
ALTER TABLE `pytania`
  ADD PRIMARY KEY (`id_pytania`);

--
-- Indexes for table `pytania_odpowiedzi`
--
ALTER TABLE `pytania_odpowiedzi`
  ADD KEY `id_pytania` (`id_pytania`),
  ADD KEY `id_odpowiedzi` (`id_odpowiedzi`);

--
-- Indexes for table `uzytkownicy`
--
ALTER TABLE `uzytkownicy`
  ADD PRIMARY KEY (`id_uzytkownika`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ankiety`
--
ALTER TABLE `ankiety`
  MODIFY `id_ankiety` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `odpowiedzi`
--
ALTER TABLE `odpowiedzi`
  MODIFY `id_odpowiedzi` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `pytania`
--
ALTER TABLE `pytania`
  MODIFY `id_pytania` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `uzytkownicy`
--
ALTER TABLE `uzytkownicy`
  MODIFY `id_uzytkownika` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `ankiety_pytania`
--
ALTER TABLE `ankiety_pytania`
  ADD CONSTRAINT `ankiety_pytania_ibfk_1` FOREIGN KEY (`id_ankiety`) REFERENCES `ankiety` (`id_ankiety`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `ankiety_pytania_ibfk_2` FOREIGN KEY (`id_pytania`) REFERENCES `pytania` (`id_pytania`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `odpowiedzi_uzytkownicy`
--
ALTER TABLE `odpowiedzi_uzytkownicy`
  ADD CONSTRAINT `odpowiedzi_uzytkownicy_ibfk_1` FOREIGN KEY (`id_uzytkownika`) REFERENCES `uzytkownicy` (`id_uzytkownika`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `odpowiedzi_uzytkownicy_ibfk_2` FOREIGN KEY (`id_odpowiedzi`) REFERENCES `odpowiedzi` (`id_odpowiedzi`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `pytania_odpowiedzi`
--
ALTER TABLE `pytania_odpowiedzi`
  ADD CONSTRAINT `pytania_odpowiedzi_ibfk_1` FOREIGN KEY (`id_pytania`) REFERENCES `pytania` (`id_pytania`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pytania_odpowiedzi_ibfk_2` FOREIGN KEY (`id_odpowiedzi`) REFERENCES `odpowiedzi` (`id_odpowiedzi`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
