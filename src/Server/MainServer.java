package Server;

import Server.model.ServerListener;

public class MainServer {
    static ServerListener server;
    public static void main(String[] args) {
        server = new ServerListener();
        server.start();
    }
}
