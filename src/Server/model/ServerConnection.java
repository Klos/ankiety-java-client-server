package Server.model;

import java.io.*;
import java.net.Socket;
import java.sql.ResultSet;
import java.sql.SQLException;

import Util.DBUtil;

public class ServerConnection extends Thread {
    DataInputStream input;
    DataOutputStream output;
    Socket clientSocket;

    public ServerConnection(Socket aClientSocket) {
        try {
            clientSocket = aClientSocket;
            // Init buffers
            input = new DataInputStream( clientSocket.getInputStream());
            output = new DataOutputStream( clientSocket.getOutputStream());
            this.start();
        }
        catch(IOException e) {
            System.out.println("ServerConnection problem: "+e.getMessage());
        }
    }

    public void run() {
        try {
            //============ TO DO LIST ====================
            // Generate new NIU
            // Get availible surveys from DB
            // Send number + survey description to client
            // Get survey's number from client
            // Get surveys questions + answers form DB
            // In loop while end of survey:
            //      send question + answers to client
            //      Get answer from client
            //      Put answer to DB
            // Send '0' to client - end of survey
            // End connection
            //=============================================
            //CreateNIU
            String stmt = "CALL pobierz_NIU();";
            ResultSet rsNIU = DBUtil.dbExecuteQuery(stmt);
            rsNIU.next();
            int userNIU = rsNIU.getInt("niu");
            System.out.println("NIU: " + userNIU);

            //get Surveys
            stmt = "SELECT * FROM ankiety WHERE dostepnosc = '1'";
            ResultSet rsSurvey = DBUtil.dbExecuteQuery(stmt);

            // Send number + survey description to client
            int numOfSurveys = 0;
            int surveyNo;
            String surveyDescription;
            while(rsSurvey.next()){
                surveyNo = rsSurvey.getInt("id_ankiety");
                surveyDescription = rsSurvey.getString("nazwa");
                output.writeInt(surveyNo);
                output.writeUTF(surveyDescription);
                numOfSurveys++;
            }
            output.writeInt(0);

            if(numOfSurveys > 0) {
                // Get survey's number from client
                int surveyId = input.readInt();
                System.out.println("Clients chose " + surveyId);

                // Get surveys questions from DB
                stmt = "SELECT * FROM ankiety_pytania ap LEFT JOIN pytania p ON ap.id_pytania = p.id_pytania WHERE id_ankiety =" + surveyId;
                ResultSet rsQuestions = DBUtil.dbExecuteQuery(stmt);

                // In loop while end of survey:
                int numOfQuestions = 0;
                int questionNo;
                String questionDescription;

                int numOfAnswers;
                int answerNo;
                String answerDescription;

                int clientAnswer;

                while(rsQuestions.next()){
                    questionNo = rsQuestions.getInt("id_pytania");
                    questionDescription = rsQuestions.getString("tresc");

                    // Get question answers from DB
                    stmt = "SELECT * FROM pytania_odpowiedzi po LEFT JOIN odpowiedzi o ON po.id_odpowiedzi = o.id_odpowiedzi WHERE id_pytania = " + questionNo;
                    ResultSet rsAnswers = DBUtil.dbExecuteQuery(stmt);
                    numOfAnswers = 0;

                    // send question to client
                    output.writeInt(questionNo);
                    output.writeUTF(questionDescription);

                    while(rsAnswers.next()){
                        answerNo = rsAnswers.getInt("id_odpowiedzi");
                        answerDescription = rsAnswers.getString("odpowiedz");
                        //      send answers to client
                        output.writeInt(answerNo);
                        output.writeUTF(answerDescription);
                        numOfAnswers++;
                    }
                    // End of answers - send '0'
                    output.writeInt(0);

                    //      Get answer from client
                    if (numOfAnswers > 0) {
                        clientAnswer = input.readInt();
                        System.out.println("Odpowiedź klienta: " + clientAnswer);

                        // Put answer to DB
                        stmt = "CALL dodaj_odpowiedz(" + userNIU+", " + clientAnswer + ")";
                        ResultSet rsAddAnswer = DBUtil.dbExecuteQuery(stmt);
                        if(rsAddAnswer.next())
                            System.out.println(rsAddAnswer.getString("Info"));
                    }
                    numOfQuestions++;
                }

                // Send '0' to client - end of survey
                output.writeInt(0);
            }

            // END OF CONNECTION
            output.flush();
        }
        catch(java.lang.NullPointerException e){
            System.out.println("NullPointerException: " + e.getMessage());
        }
        catch(IOException e) {
            System.out.println("IO: " + e.getMessage());
        }
        catch (SQLException e) {
            System.out.println("Error in SQL: " + e);
            //Return exception
        }
        finally {
            try {
                clientSocket.close();
            }
            catch (IOException e){
                System.out.println("Error while closing connection: " + e.getMessage());
            }
        }
    }

}
