package Server.model;

import Util.DBUtil;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class ServerListener extends Thread {
    private int getServerPortFromFile(){
        //TODO get server port from file
        return 55000;
    }
    @Override
    public void run() {
        try{
            // Check if DB exist
            if(DBUtil.checkAndcreateDB()) {
                int serverPort = getServerPortFromFile();
                ServerSocket listenSocket = new ServerSocket(serverPort);

                System.out.println("Server starts listening...");

                while (!Thread.currentThread().isInterrupted()) {
                    Socket clientSocket = listenSocket.accept();
                    ServerConnection c = new ServerConnection(clientSocket);
                }
            }
            else{
                System.out.println("Could not start server, DB doesn't exist");
            }
        }
        catch(IOException e) {
            System.out.println("Listen :"+e.getMessage());
        }
    }
}
