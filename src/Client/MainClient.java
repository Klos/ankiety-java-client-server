package Client;

import Client.model.ClientConnection;

public class MainClient {
    public static void main(String[] args) {
        ClientConnection connection = new ClientConnection();
        connection.run();
    }
}
