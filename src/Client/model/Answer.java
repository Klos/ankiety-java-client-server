package Client.model;

import java.util.ArrayList;

public class Answer {
    int answerNo;
    int originalAnswerNo;
    String answerDescription;

    @Override
    public String toString() {
        return ("\n" + answerNo + "\t|\t" + answerDescription);
    }

    static void showAnswerList(ArrayList<Answer> answerList){
//        System.out.println("AnswerNo\t|\tDescription");
        for(int i = 0; i < answerList.size(); i++){
            System.out.println(answerList.get(i).answerNo + "\t\t|\t" + answerList.get(i).answerDescription);
        }
        if(answerList.size() == 0)
            System.out.println("Answer's list for this question is empty ");
    }
}
