package Client.model;

import java.io.*;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Scanner;

public class ClientConnection{
    String serverIP;
    int serverPort;

    Socket serverSocket = null;

    Scanner userConsole = new Scanner(System.in);

    private void getServerInfoFromFile(){
        //TODO get server port and IP from file
        serverIP = "localhost";
        serverPort = 55000;
    }

    public void run() {
        try {
            //============ TO DO LIST ====================
            // Connect to server
            // Get availible surveys from server
            // Send survey's number to server
            // In loop while number of question will be 0 (end statement):
            //      Get question + answers from server
            //      Get answer from client
            //      Send answer to server
            // if num of question is '0' - end of survey
            // End connection
            //=============================================

            // Connect to server
            getServerInfoFromFile();
            serverSocket = new Socket(serverIP, serverPort);

            // Initialize buffers
            DataInputStream input = new DataInputStream(serverSocket.getInputStream());
            DataOutputStream output = new DataOutputStream(serverSocket.getOutputStream());

            // Get availible surveys from server
            ArrayList<Survey> surveyList = new ArrayList<Survey>();
            Survey survey = new Survey();
            while((survey.surveyNo = input.readInt()) != 0){
                survey.surveyDescription = input.readUTF();
                surveyList.add(survey);
                survey = new Survey();
            }

            // Print survey's list
            Survey.showSurveyList(surveyList);

            // if survey's list is not empty
            if(surveyList.size() > 0) {
                // Get surveyNo from user
                System.out.println("Choose survey no: ");
                int surveyNo;
                try{
                    surveyNo = userConsole.nextInt();
                } catch (java.util.InputMismatchException e){
                    userConsole.nextLine();
                    surveyNo = 0;
                }
                boolean correctValue = false;
                while (correctValue == false) {
                    // check if surveyNo in list
                    for (int i = 0; i < surveyList.size(); i++) {
                        if (surveyList.get(i).surveyNo == surveyNo) {
                            correctValue = true;
                            break;
                        }
                    }
                    if (correctValue == false) {
                        System.out.println("Wrong surveyNo, choose correct one:");
                        try{
                            surveyNo = userConsole.nextInt();
                        } catch (java.util.InputMismatchException e){
                            userConsole.nextLine();
                            surveyNo = 0;
                        }
                    }
                }
                // Send survey's number to server
                output.writeInt(surveyNo);
                surveyList.clear();

                Question question = new Question();
                ArrayList<Answer> answerList = new ArrayList<Answer>();

                // In loop while number of question will be 0 (end statement):
                // if num of question is '0' - end of survey
                int numOfQuestions = 0;
                while ((question.questionNo = input.readInt()) != 0) {
                    // Get question from server
                    question.questionDescription = input.readUTF();
                    System.out.println(question.questionDescription);

                    Answer answer = new Answer();
                    int answerIterator = 1;

                    // Get answers from server
                    while ((answer.originalAnswerNo = input.readInt()) != 0) {
                        answer.answerDescription = input.readUTF();
                        answer.answerNo = answerIterator;
                        answerIterator++;
                        answerList.add(answer);
                        answer = new Answer();
                    }

                    // Print answer's list
                    Answer.showAnswerList(answerList);
                    //if answer's list is not empty
                    if(answerList.size() > 0) {
                        //      Get answer from client
                        System.out.println("Choose answer: ");
                        int answerNo;
                        try{
                            answerNo = userConsole.nextInt();
                        } catch (java.util.InputMismatchException e){
                            userConsole.nextLine();
                            answerNo = 0;
                        }
                        correctValue = false;
                        int originalAnswerNo = 0;
                        while (correctValue == false) {
                            // check if answerNo in list
                            for (int i = 0; i < answerList.size(); i++) {
                                if (answerList.get(i).answerNo == answerNo) {
                                    correctValue = true;
                                    originalAnswerNo = answerList.get(i).originalAnswerNo;
                                    break;
                                }
                            }
                            if (correctValue == false) {
                                System.out.println("Wrong answerNo, choose correct one:");
                                try{
                                    answerNo = userConsole.nextInt();
                                } catch (java.util.InputMismatchException e){
                                    userConsole.nextLine();
                                    answerNo = 0;
                                }
                            }
                        }
                        // Send answer to server
                        output.writeInt(originalAnswerNo);
                        answerList.clear();
                    }
                    numOfQuestions++;
                }
                if(numOfQuestions > 0)
                    System.out.println("You have answered for " + numOfQuestions + " questions");
                else
                    System.out.println("This survey have no questions. It is empty.");
            }
            output.flush();
            // END OF CONNECTION


        }
        catch(java.lang.NullPointerException e){
            System.out.println("NullPointerException: " + e.getMessage());
        }
        catch (UnknownHostException e) {
            System.out.println("Socket: " + e.getMessage());
        }
        catch (IOException e) {
            System.out.println("IO: " + e.getMessage());
            if(e.getMessage().equals("Connection refused: connect"))
                System.out.println("Server is unavailable");
            else
                System.out.println(e.getMessage());
        }
        finally {
            // Close connections
            if (serverSocket != null)
                try {
                    serverSocket.close();
                }
                catch (IOException e) {
                    System.out.println("Error while closing connection: " + e.getMessage());
                }
        }

        System.out.println("\nEnd of survey connection");
    }
}
