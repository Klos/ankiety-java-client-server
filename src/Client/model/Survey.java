package Client.model;

import java.util.ArrayList;

public class Survey {
    int surveyNo;
    String surveyDescription;

    @Override
    public String toString() {
        return ("\n" + surveyNo + "\t|\t" + surveyDescription);
    }

    static void showSurveyList(ArrayList<Survey> surveyList){
//        System.out.println("SurveyNo\t|\tDescription");
        System.out.println("Surveys List:");
        for(int i = 0; i < surveyList.size(); i++){
            System.out.println(surveyList.get(i).surveyNo + "\t\t|\t" + surveyList.get(i).surveyDescription);
        }
        if(surveyList.size() == 0)
            System.out.println("List of available surveys is empty");
    }
}
