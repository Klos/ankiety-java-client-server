package Util;

import com.sun.rowset.CachedRowSetImpl;

import javax.sql.rowset.CachedRowSet;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.sql.*;
import java.util.Properties;

public class DBUtil {
    // Declare JDBC Driver
    private static final String DB_DRIVER = "com.mysql.jdbc.Driver";

    // Connection
    private static Connection conn = null;

    // Login data
    private static final String username = "root";
    private static final String pass = "";
    private static final String ip = "localhost";
    private static final String port = "3306";
    private static final String sid = "ankiety_wadolny";
    private static final String mysqldumpExePath = "C:/xampp/mysql/bin/mysqldump.exe";
    private static final String mysqlExePath = "C:/xampp/mysql/bin/mysql.exe";

    //Connection String
    private static final String connStr = String.format("jdbc:mysql://%s:%s/%s", ip, port, sid);
    // That format is needed for calling stored procedures
    private static final String connStrLogin = String.format("jdbc:mysql://%s:%s/%s?user=%s&password=%s", ip, port, sid, username, pass);
    private static final String connStrLoginNoDB = String.format("jdbc:mysql://%s:%s/?user=%s&password=%s", ip, port, username, pass);

    // Connect to DB
    public static void dbConnect() throws SQLException {
        // setting Oracle driver
        try {
            Class.forName(DB_DRIVER);
//            System.out.println(connStr);
        } catch (ClassNotFoundException e) {
            System.err.printf("Where is your JDBC driver?");
            e.printStackTrace();
        }
//        System.out.println("DB Driver Registered!");

        // Establish the Oracle Connection
        try {
            conn = DriverManager.getConnection(connStrLogin);
        } catch (SQLException e) {
            System.err.println("Connection failed!");
            e.printStackTrace();
        }
    }
    // Connect to MySQL
    public static void mysqlConnect() throws SQLException {
        // setting Oracle driver
        try {
            Class.forName(DB_DRIVER);
//            System.out.println(connStr);
        } catch (ClassNotFoundException e) {
            System.err.printf("Where is your JDBC driver?");
            e.printStackTrace();
        }
        System.out.println("DB Driver Registered!");

        // Establish the MYSQL Connection
        try {
            conn = DriverManager.getConnection(connStrLoginNoDB);
            System.out.println("Connected");
        } catch (SQLException e) {
            System.err.println("Connection failed!");
            e.printStackTrace();
        }
    }

    // Close connection
    public static void dbDisconnect() throws SQLException {
        try {
            if (conn != null && !conn.isClosed()) {
                conn.close();
            }
        } catch (SQLException e) {
            System.err.println("Error while closing the connection!");
            e.printStackTrace();
        }
    }

    // DB Execute Query Operation (select)
    public static ResultSet dbExecuteQuery(String queryStmt) throws SQLException {
        Statement stmt = null;
        ResultSet resultSet = null;
        CachedRowSet cachedRowSet = null;

        try {
            // Connect to DB
            dbConnect();
//            System.out.println("Select statement: " + queryStmt);

            // Create statement
            stmt = conn.createStatement();
//            stmt.execute("SET NAMES 'unicode'");

            // Execute operation
            resultSet = stmt.executeQuery(queryStmt);

            // Cached Row Set Implementation
            // In order to prevent "java.sql.SQLRecoverableException: Closed Connection: next error
            cachedRowSet = new CachedRowSetImpl();
            cachedRowSet.populate(resultSet);
        } catch (SQLException e) {
            System.err.println("Error at executing query");
            e.printStackTrace();
        } finally {
            if (resultSet != null) {
                resultSet.close();
            }
            if (stmt != null) {
                stmt.close();
            }
            // Close connection
//            conn.close();
            dbDisconnect();
        }
        return cachedRowSet;
    }
    public static void dbExecuteUpdate(String sqlStmt) throws SQLException {
        Statement stmt = null;
        try {
            dbConnect();
            stmt = conn.createStatement();
//            stmt.execute("SET NAMES 'utf8'");
            stmt.executeUpdate(sqlStmt);
        } catch (SQLException e) {
            System.err.println("Error while executing update statement");
            e.printStackTrace();
        } finally {
            if(stmt != null) {
                stmt.close();
            }
            dbDisconnect();
        }
    }

    public static boolean checkAndcreateDB(){
       if(checkIfDBExist(sid) == false){
           return loadDBDump();
       }
       else
           return true;
    }
    public static void createDB(){
        // create only db named ankiety_wadolny
        try {
            DBUtil.mysqlConnect();
            String sqlStmt = "CREATE DATABASE " + sid;
            Statement stmt = conn.createStatement();
            stmt.executeUpdate(sqlStmt);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        finally{
            try {
                DBUtil.dbDisconnect();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
    public static void createDBDump(){
        String executeCmd;
        if(pass.isEmpty())
            executeCmd = mysqldumpExePath + " -u " + username + " --routines --add-drop-database -B " + sid + " -r " + sid + ".sql";
        else
            executeCmd = mysqldumpExePath + " -u " + username + " -p" + pass + " --routines --add-drop-database -B " + sid + " -r " + sid + ".sql";

        System.out.println(executeCmd);
        Process runtimeProcess;
        try {
            runtimeProcess = Runtime.getRuntime().exec(executeCmd);
            int processComplete = runtimeProcess.waitFor();
            if (processComplete == 0) {
                System.out.println("Backup created successfully");
            } else {
                System.out.println("Could not create the backup");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean loadDBDump(){
        if(checkIfDBExist(sid) == false)
            createDB();
        //LOAD DUMP
        String executeCmd;
        if(pass.isEmpty())
            executeCmd = "cmd /c \"C:/xampp/mysql/bin/mysql.exe -u " + username + " " + sid + " < " + sid + ".sql\"";
        else
            executeCmd = "cmd /c \"C:/xampp/mysql/bin/mysql.exe -u " + username + " -p " + pass + " " + sid + " < " + sid + ".sql\"";

        Process runtimeProcess;
        try {
            runtimeProcess = Runtime.getRuntime().exec(executeCmd);
            int processComplete = runtimeProcess.waitFor();
            if (processComplete == 0) {
                System.out.println("Backup restored successfully");
                return true;
            } else {
                System.out.println("Could not restore the backup");
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static boolean checkIfDBExist(String dbName){
        try{
            DBUtil.mysqlConnect();
            ResultSet resultSet = conn.getMetaData().getCatalogs();
            while (resultSet.next()) {
                String databaseName = resultSet.getString(1);
                if(databaseName.equals(dbName)){
                    return true;
                }
            }
            resultSet.close();
        }
        catch(Exception e){
            e.printStackTrace();
        }
        finally{
            try{
                DBUtil.dbDisconnect();
            }
            catch(Exception e){
                e.printStackTrace();
            }
        }
        return false;
    }
}
